﻿
module.exports.setRoutes = (dishRouter) => {

    dishRouter.route('/')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Deleing all dishes');
        })
        .get(function (request, response, next) {
            response.end('Will send all dishes to you');
        })
        .post(function (request, response, next) {
            response.end('Will add the dish ' + request.body.name + ' with details: ' + request.body.description);
        });

    dishRouter.route('/:dishID')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Will delete dish # ' + request.params.dishID);
        })
        .get(function (request, response, next) {
            response.end('Will send dish # ' + request.params.dishID);
        })
        .put(function (request, response, next) {
            response.write('Updating Dish # ' + request.params.dishID + '\n');
            response.end('Will update dish ' + request.body.name + ' with details ' + request.body.description);
        });

}