﻿module.exports.setRoutes = (leaderRouter) => {

    leaderRouter.route('/')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Deleing all leaders');
        })
        .get(function (request, response, next) {
            response.end('Will send all leaders to you');
        })
        .post(function (request, response, next) {
            response.end('Will add the leader ' + request.body.name + ' with details: ' + request.body.description);
        });

    leaderRouter.route('/:leaderID')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Will delete leader # ' + request.params.leaderID);
        })
        .get(function (request, response, next) {
            response.end('Will send leader # ' + request.params.leaderID);
        })
        .put(function (request, response, next) {
            response.write('Updating leader # ' + request.params.leaderID + '\n');
            response.end('Will update leader ' + request.body.name + ' with details ' + request.body.description);
        });

}