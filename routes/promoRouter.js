﻿module.exports.setRoutes = (promoRouter) => {
    
    promoRouter.route('/')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Deleing all promos');
        })
        .get(function (request, response, next) {
            response.end('Will send all promos to you');
        })
        .post(function (request, response, next) {
            response.end('Will add the promo ' + request.body.name + ' with details: ' + request.body.description);
        });

    promoRouter.route('/:promoID')
        .all(function (request, response, next) {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            next();
        })
        .delete(function (request, response, next) {
            response.end('Will delete promo # ' + request.params.promoID);
        })
        .get(function (request, response, next) {
            response.end('Will send promo # ' + request.params.promoID);
        })
        .put(function (request, response, next) {
            response.write('Updating promo # ' + request.params.promoID + '\n');
            response.end('Will update promo ' + request.body.name + ' with details ' + request.body.description);
        });

}